== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Artengo Aguado, Felipe
* Benítez González, Iván
* Cañizares Romero, Ángel
* Delgado Jiménez, Jesús
* Domínguez Muñoz, Alejandro
* Durán García, Raúl
* Garcia Moreno, Fermin
* Heredia Pérez, Elena
* Luque Giráldez, José Rafael
* Meléndez Muñoz, Salvador
* Rodríguez Medina, José Francisco
* Rubio Carballo, Pablo
* Silvestre Mérida, Emilio
* Tejera García, Juan Manuel

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Peugeot 508 (5 plazas)

* Alejandro Domínguez
* José Francisco
* Rafael Luque
* Fermin Garcia
* Felipe Artengo

==== Ford Tourneo (7 plazas)

* Ángel Cañizares
* Iván Benítez
* Jesús Delgado
* Pablo Rubio
* Salvador Meléndez
* Emilio Silvestre

==== Renault 11 (5 plazas, 2 con cinturón)

* Raúl Durán
* Elena Heredia 
